<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeControllers/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//  ============== User =================
$route['index'] = 'HomeControllers/index';

$route['contactus'] = 'ContactusControllers';
$route['scomments'] = 'ContactusControllers/saveCommentsByCus';

$route['aboutas'] = 'AboutasControllers';
$route['gallery'] = 'GalleryControllers';
$route['promotions'] = 'PromotionsControllers';

$route['booking'] = 'BookingControllers/ShowAllBookingPageByBranch/Content0';
$route['booking/(:any)'] = 'BookingControllers/ShowAllBookingPageByBranch/$1';
$route['bookings/(:any)'] = 'BookingControllers/ShowAllBookingPageByBranchs/$1';
$route['showdesbookingbybranch/(:any)'] = 'BookingControllers/showDescriptionByBranch/$1';
$route['searchpbooking'] = 'BookingControllers/searchBookingPage';
$route['viewde/(:any)'] = 'BookingControllers/viewDetailByidb/$1';
$route['mybooking'] = 'BookingControllers/bookingFormAPI';

$route['subindex'] = 'HomeControllers/subindex';
$route['sbtypesubtype/(:any)'] = 'HomeControllers/selectBranchBytypeSubtype/$1';

//  ===================================

// ============== Main page =================
$route['sldelete'] = 'employee/MainpageController/slDelete';
$route['slundo'] = 'employee/MainpageController/slUndo';
$route['slsave'] = 'employee/MainpageController/saveSlideShow';
// ==========================================

// ============== About as page =================
$route['saboutas'] = 'employee/AboutasController/saveAboutas';
$route['dgaboutas'] = 'employee/AboutasController/deleteGroupAboutas';
$route['showaboutasbyid/(:any)'] = 'employee/AboutasController/showbyidAboutas/$1';
$route['eaboutas'] = 'employee/AboutasController/editAboutas';
$route['delaboutas/(:any)'] = 'employee/AboutasController/delbyidAboutas/$1';
$route['daboutas'] = 'employee/AboutasController/deleteAboutas';
// ==============================================

//  ============== Booking page ==============
$route['showbooking'] = 'BookingControllers/ShowBookingByBranch';
$route['showbyroomtype/(:any)'] = 'BookingControllers/showbyRoomType/$1';
$route['showbookingbid/(:any)'] = 'AboutasControllers/showAboutasByID/$1';

$route['showpbookingbyid/(:any)'] = 'employee/BookingusepageController/showbyidPageBooking/$1';
$route['savebookingusepage'] = 'employee/BookingusepageController/saveBookingUsePage';
$route['editbookingusepage'] = 'employee/BookingusepageController/editBookingUsePage';
$route['delbookingusepagebyid/(:any)'] = 'employee/BookingusepageController/delbyidBookingUsePage/$1';
$route['delbookingusepage'] = 'employee/BookingusepageController/deleteBookingUsePage';
$route['dgbookingusepage'] = 'employee/BookingusepageController/deleteGroupBookingUsePage';


//  =====================================

//  ============== Employee ==============
$route['empmain'] = 'employee/EmpmainpageControllers';
//$route['empbranch'] = 'employee/BranchmanagementControllers';

$route['empmain/(:any)'] = 'employee/EmpmainpageControllers/empcontent/$1';
$route['editbranch/(:any)'] = 'employee/EmpmainpageControllers/showBranchByID/$1';
$route['delbranch/(:any)'] = 'employee/EmpmainpageControllers/deleteBranchByID/$1';
//  ===================================

//  ============== Gallery ==============
$route['simggroup'] = 'employee/GalleryController/saveImgGroup';
$route['showgroupbybimg/(:any)'] = 'employee/GalleryController/showGroupByImg/$1';
$route['sbpicgallery'] = 'employee/GalleryController/saveImgToGallery';
$route['dggallery'] = 'employee/GalleryController/deleteGroupGallery';
$route['dgallery/(:any)'] = 'employee/GalleryController/delbyidGallery/$1';
$route['delgallery'] = 'employee/GalleryController/deleteGallery';
$route['showgallerybyid/(:any)'] = 'employee/GalleryController/showbyidGallery/$1';
$route['editgallery'] = 'employee/GalleryController/editGallery';

$route['deleteimggallery/(:any)'] = 'employee/GalleryController/deleteImgGalleryp/$1';
$route['sdeletegroupgallery/(:any)'] = 'employee/GalleryController/showDeletegroupgallery/$1';
$route['dgpgallery'] = 'employee/GalleryController/deleteGroupPicGallery';
//  ======================================

//  ============== Customer ==============
  $route['savecustomer'] = 'employee/EmpmainpageControllers/saveCustomer';
  $route['editcustomer/(:any)'] = 'employee/EmpmainpageControllers/showCustomerByID/$1';
  $route['delcustomer/(:any)'] = 'employee/EmpmainpageControllers/deleteCustomerByID/$1';
//  ===================================

//  ============== Picture ==============
  $route['infopicture'] = 'employee/PictureController/InfoPictureBranch';
  $route['sbpicture'] = 'employee/PictureController/savePictureBranch';
  $route['showpicbyid/(:any)'] = 'employee/PictureController/showbyidPicture/$1';
  $route['delpicbyid/(:any)'] = 'employee/PictureController/delbyidPicture/$1';
  $route['editpicture'] = 'employee/PictureController/editPicture';
  $route['delpicture'] = 'employee/PictureController/deletePicture';
  $route['dgpicture'] = 'employee/PictureController/deleteGroupPicture';
//  =====================================

//  ============== System ==============
$route['login'] = 'ssystem/LoginController/loginMe';
$route['logout'] = 'ssystem/LoginController/logout';

$route['forgotpassword'] = 'ssystem/ForgotpasswordController/ForgotPassword';
$route['resetpassword'] = 'ssystem/ForgotpasswordController/ResetPassword';
//  ===================================



//  ============== Application Programming Interface ==============
$route['infobranch'] = 'yotakaapi/BranchController/infoBranch';
$route['ebranch'] = 'yotakaapi/BranchController/editBranch';
$route['sbranch'] = 'yotakaapi/BranchController/saveBranch';
$route['dbranch'] = 'yotakaapi/BranchController/deleteBranch';
$route['dgbranch'] = 'yotakaapi/BranchController/deleteBranchByGroup';
$route['showbyid/(:any)'] = 'yotakaapi/BranchController/showbyidBranch/$1';

$route['cusregister'] = 'yotakaapi/CustomerController/RegisterCustomer'; //no api

$route['infocustomer'] = 'yotakaapi/CustomerController/infoCustomer';
$route['scustomer'] = 'yotakaapi/CustomerController/saveCustomer';
$route['ecustomer'] = 'yotakaapi/CustomerController/editCustomer';
$route['dcustomer'] = 'yotakaapi/CustomerController/deleteCustomer';
$route['dgcustomer'] = 'yotakaapi/CustomerController/deleteCustomerByGroup';
$route['showcusbyid/(:any)'] = 'yotakaapi/CustomerController/showbyidCustomer/$1';
$route['showcusbyidc/(:any)'] = 'yotakaapi/CustomerController/showbyIDC/$1';

$route['infocomment'] = 'yotakaapi/CommentController/infoComment';
$route['sentcomment/(:any)'] = 'yotakaapi/CommentController/sentCommentToCustomer/$1';
$route['savecomment'] = 'yotakaapi/CommentController/saveToCustomer';

$route['usecase/(:any)'] = 'yotakaapi/UsecaseController/SelectUsecase/$1';
$route['myapi'] = 'yotakaapi/MyuserguideController/index';
//  ===================================================
