<?php

/*
 * Object
 */
$lang['close'] = 'Close';
$lang['delete'] = 'Delete';
$lang['notdelete'] = 'Can not delete';
$lang['deleteselect'] = 'Do you want to delete the selected data?';
$lang['save'] = 'Save';
$lang['edit'] = 'Edit';
$lang['continue'] = 'Continue';
$lang['confdeletestart'] = 'Do you want to delete';
$lang['confdeleteend'] = 'Yes or No';
$lang['select'] = 'Selected';
$lang['nodelete'] = 'No items selected.';
$lang['mypic'] = 'Picture';
$lang['signout'] = 'Sign out';
$lang['logout'] = 'Logout';
$lang['signin'] = 'Sign in';
$lang['editprofile'] = 'Edit Profile';
$lang['sent'] = 'Sent';
$lang['search'] = 'Search';
$lang['showmore'] = 'Show more';
$lang['no'] = 'No.';
$lang['confirmdeletion'] = 'Confirm deletion';
$lang['selectitem'] = 'Select item';

//  -------------------------------------------------- * Top menu
$lang['home'] = 'HOME';
$lang['aboutas'] = 'ABOUT AS';
$lang['promotions'] = 'PROMOTIONS';
$lang['gallery'] = 'GALLERY';
$lang['contactus'] = 'CONTACT US';
$lang['booking'] = 'BOOKING';

#  Modal user
$lang['user'] = 'User';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['passwordconf'] = 'Confirm password';
$lang['forgot_your_password'] = 'Forgot your password';
$lang['register'] = 'Register';
//$lang['please_sign_in'] = 'Please sign in';
#  Dropdown language
$lang['please_select_language'] = 'Please select language';
$lang['english'] = 'English';
$lang['thailand'] = 'Thailand';
$lang['chinese'] = 'Chinese';
$lang['japanese'] = 'Japanese';

#  Modal forgot your password
$lang['write_your_email_here'] = 'Write your email here';

#  Modal register
#  ???????
//  -------------------------------------------------- * Top menu
#####################################
//  -------------------------------------------------- * Footer
$lang['contact'] = 'CONTACT';
$lang['yotaka_adr'] = '264/5 Soi Mahadthai Ramkhamheang 65 Ladprao 122 Wangtonglang Bangkok';
$lang['about'] = 'ABOUT';
$lang['usefullinks'] = 'USEFUL LINKS';
//  -------------------------------------------------- * Footer
#####################################
//  -------------------------------------------------- * Contact us
$lang['comments'] = 'Comments';
$lang['cname'] = 'Your name';
$lang['vote'] = 'Customer review';
$lang['status'] = 'Status';
//  -------------------------------------------------- * Contact us

//  -------------------------------------------------- * Register
$lang['regisheading'] = 'Register';
$lang['idcard'] = 'ID Card / Passport ID';
$lang['titlename'] = 'Title name';
$lang['fristname'] = 'Frist name';
$lang['lastname'] = 'Last name';
$lang['address'] = 'Address';
$lang['brithday'] = 'Brith day';
$lang['email'] = 'E-mail';
$lang['phonenumber'] = 'Telephone No.';
$lang['confirmpassword'] = 'Re-enter Password';
//  -------------------------------------------------- * Register

//  -------------------------------------------------- * Branch
$lang['branchcode'] = 'Branch code';
$lang['vatnumber'] = 'Tax ID';
$lang['branchnameEN'] = 'Branch name ENG';
$lang['branchnameTH'] = 'Branch name TH';
$lang['establishment'] = 'Establishment';
$lang['logopicture'] = 'Trade Mark';
$lang['zipcode'] = 'Zip code';
$lang['editbranch'] = 'Edit branch data';
$lang['delbranch'] = 'Delete branch data';
$lang['branchhead'] = 'Insert branch data';
//  -------------------------------------------------- * Branch

//  -------------------------------------------------- * Customer
$lang['customomerhead'] = 'Insert customer data';
$lang['customomerheadedit'] = 'Edit customer data';
$lang['customomercode'] = 'Customer code';
$lang['customomername'] = 'Customer name';
$lang['customomertype'] = 'Customer type';
$lang['delcustomer'] = 'Delete Customer data';

//  -------------------------------------------------- * Customer

//  -------------------------------------------------- * booking
$lang['branchname'] = 'Branch';
$lang['checkin'] = 'Check in';
$lang['noofguest'] = 'No. of guest';
$lang['bbooking'] = 'Booking';
$lang['description'] = 'Description';
$lang['details'] = 'Details';
$lang['note'] = 'Note';
$lang['editdate'] = 'Edit (date)';
$lang['selectbranch'] = 'Please select branch';
$lang['descriptth'] = 'Description TH';
$lang['descripten'] = 'Description EN';
$lang['detailsth'] = 'Details TH';
$lang['detailsen'] = 'Details EN';
$lang['releasedate'] = 'Release Date';
$lang['roomtype'] = 'ROOMS TYPE';
//  -------------------------------------------------- * booking


//  -------------------------------------------------- * about as
$lang['title'] = 'Title';
$lang['story'] = 'Story';
$lang['titleth'] = 'Title TH';
$lang['titleen'] = 'Title EN';
$lang['storyth'] = 'Story TH';
$lang['storyen'] = 'Story EN';
//  -------------------------------------------------- * about as

//  -------------------------------------------------- * gallery
$lang['groupno'] = 'Group No.';
$lang['groupnameth'] = 'Group TH';
$lang['groupnameen'] = 'Group EN';
$lang['groupname'] = 'Group name';
$lang['pleaseaddgroup'] = 'Please add a group';
$lang['grouphead'] = 'Insert group';
$lang['groupedit'] = 'Edit group';
$lang['groupdelete'] = 'Delete group';
//  -------------------------------------------------- * gallery

//  -------------------------------------------------- * slide
$lang['slidehead'] = 'Insert Picture';
//  -------------------------------------------------- * slide

//  -------------------------------------------------- * comment
$lang['commenthead'] = 'Reply comment';
$lang['reply'] = 'Reply';
//  -------------------------------------------------- * comment

//  -------------------------------------------------- * Content
$lang['contenthead'] = 'Insert Content';
$lang['contentedit'] = 'Edit Content';
$lang['contentdeletet'] = 'Delete Content';
//  -------------------------------------------------- * Content
