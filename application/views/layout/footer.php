<style>
.open-button {
  background-color: #ff4d4d;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
  z-index: 20;
}
</style>

<?php if (isset($_SESSION['isLoggedIn'])): ?>
    <button class="open-button" data-toggle="modal" data-target="#myBookingModal">My Booking<b>(8)</b></button>
<?php endif; ?>


<!-- The Modal -->
  <div class="modal fade" id="myBookingModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content font-bg">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">My Booking</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body font-bg2">
            <h2>Hover Rows</h2>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>วันที่</th>
                  <th>รายละเอียด</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>John</td>
                  <td>Doe</td>
                  <td>john@example.com</td>
                </tr>
                <tr>
                  <td>Mary</td>
                  <td>Moe</td>
                  <td>mary@example.com</td>
                </tr>
                <tr>
                  <td>July</td>
                  <td>Dooley</td>
                  <td>july@example.com</td>
                </tr>
              </tbody>
            </table>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer font-bg">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  <!-- The Modal -->

<!-- Footer -->
<footer class="page-footer font-small blue-grey lighten-5 mt-4 font-bg">

    <div style="background-color: #313A45;">
        <div class="container ">

            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">

            </div>
            <!-- Grid row-->

        </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5 font-bg">

        <!-- Grid row -->
        <div class="row mt-3 dark-grey-text">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

                <img src="<?php echo base_url(); ?>assets/img/logo2.png" alt="logo" style="width: 100%; height: auto; position: relative;">

            </div>
            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?php echo $this->lang->line("contact"); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <img src="<?php echo base_url(); ?>assets/img/iconn-07.png" alt="logo" style="width:20px;">
                    </i><?php echo $this->lang->line("yotaka_adr"); ?></p>
                <p>
                    <img src="<?php echo base_url(); ?>assets/img/iconn-08.png" alt="logo" style="width:20px;">
                    02-9342720</p>
                <p>
                    <img src="<?php echo base_url(); ?>assets/img/iconn-05.png" alt="logo" style="width:20px;">
                    yotakagroup_hotel@hotmail.com</p>

            </div>
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?php echo $this->lang->line("about"); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a class="dark-grey-text" style="color: white"  href="<?php echo base_url(); ?>HomeControllers"><?php echo $this->lang->line("home"); ?></a>
                </p>
                <p>
                    <a class="dark-grey-text" style="color: white" href="<?php echo base_url(); ?>aboutas"><?php echo $this->lang->line("aboutas"); ?></a>
                </p>
                <!-- <p>
                    <a class="dark-grey-text" style="color: white" href="<?php echo base_url(); ?>promotions" ><?php echo $this->lang->line("promotions"); ?></a>
                </p> -->
                <p>
                    <a class="dark-grey-text" style="color: white" href="<?php echo base_url(); ?>booking" ><?php echo $this->lang->line("booking"); ?></a>
                </p>
                <p>
                    <a class="dark-grey-text" style="color: white" href="<?php echo base_url(); ?>gallery"><?php echo $this->lang->line("gallery"); ?></a>
                </p>
                <p>
                    <a class="dark-grey-text" style="color: white" href="<?php echo base_url(); ?>contactus"><?php echo $this->lang->line("contactus"); ?></a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?php echo $this->lang->line("usefullinks"); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="https://www.facebook.com/yotaka122/" class="dark-grey-text" style="color:white;">FACEBOOK</a>
                </p>
                <!-- <p>
                    <a href="https://www.traveloka.com" class="dark-grey-text">Traveloka</a>
                </p>
                <p>
                    <a href="https://www.expedia.co.th" class="dark-grey-text">Expedia</a>
                </p> -->
<!--                <p>
                    <a class="dark-grey-text">Help</a>
                </p>-->

            </div>
            <!-- Grid column -->

            <!-- Grid column -->

            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center text-white-50 py-6" >
        <div style="background-color: #000000; height: 80px; "><br>©2018 © YOTAKA GROUP.ALL RIGHT RESAVED.</div>
    </div>
    <!-- Copyright -->

</footer>

<script>
// function openForm() {
//     document.getElementById("myForm").style.display = "block";
// }

// function closeForm() {
//     document.getElementById("myForm").style.display = "none";
// }
</script>
