<?php

class RoomModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function SaveDataFromAPI($d) {
        $this->db->insert('ROM', $d);
    }

    public function DeleteDataFromAPI($d) {

        $data = array(
            'ROMdelete' => '1',
            'ROMdeleteBy' => 'DAPI'
        );

        $this->db->where('ROMno', $d['ROMno']);
        $this->db->where('ROMbrhid', $d['ROMbrhid']);
        $this->db->update('BRH', $data);
    }
    
    

}
