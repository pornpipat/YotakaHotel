<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EmpmainpageControllers extends BaseController {

    public function __construct() {
        parent::__construct();

        $this->SystemControl = new SystemControl();

        $this->load->model('SystemModel');
        // $this->load->model('RoomModel');
        $this->load->helper('cookie');

        $MyLang = $this->SystemControl->CheckYourIPapi();
        $country = strtolower($MyLang->geoplugin_countryName);
        if (!isset($_COOKIE['lang'])) {
            if ($country == 'thailand') {
                $lang = 'thailand';
            } else {
                $lang = 'english';
            }
            setcookie('lang', $lang);
        } else {
            $lang = $_COOKIE['lang'];
        }
        $this->lang->load($lang, $lang);
    }

    public function index() {
        $cdata['xurl'] = '';

        $mp['startpage'] = $this->load->view('layout/emp/startpage', '', TRUE);
        $mp['menu'] = $this->load->view('layout/emp/menu', '', TRUE);

        $mp['content'] = $this->load->view('employee/branchmanagement', $cdata, TRUE);

        $mp['footer'] = $this->load->view('layout/emp/footer', '', TRUE);
        $mp['endpage'] = $this->load->view('layout/emp/endpage', '', TRUE);
        $this->load->view('employee/empmainpage', $mp);
    }

    public function empcontent($menunumber) {
        $this->load->model('SystemModel');
        $param = '?param=true';

        switch ($menunumber) {
            case 'M0000':
                $cview = 'mainpage';
                // $myapi = 'infobranch' . $param;
                // $rapi = gUrl($myapi, 'empmain/');
                $cdata['datafromapi'] = cUrl($this->config->item('apiBranch'), 'get');
                break;

            case 'M0001':
                $this->load->model('MainpageModel');
                // $this->load->model('BookingusepageModel');

                $cview = 'mage_mainpage_slideshow';
                $cdata['pslide'] = $this->MainpageModel->InfoSlide();
                $cdata['xdate'] = $this->SystemModel->MaxDateTime('BRH');
                $cdata['startpage'] = $this->load->view('layout/startpage', '', TRUE);
                $cdata['endpage'] = $this->load->view('layout/endpage', '', TRUE);
              break;

            case 'M1001':
                $cview = 'branchmanagement';
                // $myapi = 'infobranch' . $param;
                // $rapi = gUrl($myapi, 'empmain/');
                // debug($rapi);
                // exit();
                $cdata['xdate'] = $this->SystemModel->MaxDateTime('BRH');
                $cdata['datafromapi'] = cUrl($this->config->item('apiBranch'), 'get');
                break;

            case 'M2001':
                $cview = 'customermanagement';
                // $myapi = 'infocustomer' . $param;
                // $rapi = gUrl($myapi, 'empmain/');
                // debug($rapi);
                // exit();
                $cdata['xdate'] = $this->SystemModel->MaxDateTime('CUS');
                $cdata['custype'] = $this->SystemModel->Usecase('11');
                $cdata['titlename'] = $this->SystemModel->Usecase('13');
                $cdata['datafromapi'] = cUrl($this->config->item('apiCustomer'), 'get');

                break;

            case 'M3001':
                $cview = 'roommanagement';
                $rapi = 'http://122.155.201.37/adminYotaka/api/rooms/get_roomallroomoption?data=1'; //adminAPI('get_rooms');

                $cdata['xdate'] = $this->SystemModel->MaxDateTime('ROM');

                $myapib = 'infobranch' . $param;
                $bdata = gUrl($myapib, 'empmain/');
                $cdata['branch'] = cUrl($bdata, 'get');
                $cdata['room'] = cUrlAdmin($rapi);
                debug($cdata['room']);
                exit();
                $cdata['datafromapi'] = cUrl($rapi, 'get');
                break;

            case 'M4001':
                $cview = 'commentmanagement';
                // $myapi = 'infocomment' . $param;
                // $rapi = gUrl($myapi, 'empmain/');

                $cdata['custype'] = $this->SystemModel->Usecase('29');
                $cdata['datafromapi'] = cUrl($this->config->item('apiComment'), 'get');
                break;

            case 'M5001':
                $this->load->model('BranchModel');

                $cview = 'bookingmanagementpic';
                $rapi = adminAPI('get_rooms');
                $myapib = 'infobranch' . $param;
                $bdata = gUrl($myapib, 'empmain/');
                $myapip = 'infopicture' . $param;
                $pdata = gUrl($myapip, 'empmain/');

                $cdata['branch'] = cUrl($this->config->item('apiBranch'), 'get');
                $cdata['xdate'] = $this->SystemModel->MaxDateTimeWhere('PIC', 1);
                $cdata['datafromapi'] = cUrl($this->config->item('apiBookingPic'), 'get');
                break;

            case 'M5002':
                $this->load->model('BookingusepageModel');
                $cview = 'bookingmanagementpbooking';
                // $myapib = 'infobranch' . $param;
                // $bdata = gUrl($myapib, 'empmain/');

                $cdata['branch'] = cUrl($this->config->item('apiBranch'), 'get');
                $cdata['xdate'] = $this->SystemModel->MaxDateTime('PU03');
                $cdata['datafromapi'] = $this->BookingusepageModel->infoBookingUsePage();
                // debug($cdata['datafromapi']);
                // exit();
              break;

            case 'M6001':
              $this->load->model('AboutasModel');
              $cview = 'aboutasmanagement';
              $cdata['xdate'] = $this->SystemModel->MaxDateTime('PU01');
              $cdata['allaboutas'] = $this->AboutasModel->infoAboutas();
              // debug($cdata['allaboutas']);
              // exit();
              break;

            case 'M7001':
              $this->load->model('GalleryModel');
              $cview = 'gallerymanagement';
              // $myapib = 'infobranch' . $param;
              // $bdata = gUrl($myapib, 'empmain/');
              $cdata['branch'] = cUrl($this->config->item('apiBranch'), 'get');
              $cdata['dgallery'] = $this->GalleryModel->infoGallery();
              $cdata['xdate'] = $this->SystemModel->MaxDateTime('PU04');
              break;

            case 'M7002':
              $this->load->model('GalleryModel');
              $cview = 'gallerymanagementpic';

              // $myapib = 'infobranch' . $param;
              // $bdata = gUrl($myapib, 'empmain/');
              $cdata['branch'] = cUrl($this->config->item('apiBranch'), 'get');
              $cdata['dgallery'] = $this->GalleryModel->infoGallery();
              $cdata['xdate'] = $this->SystemModel->MaxDateTime('PU04');
              break;

            case 'M8001':
                $cview = 'promotionmanagement';
                break;

            default:
                echo 'Press check your menu number file --> views/layout/emp/menu.php';
                exit();
                break;
        }

        if ($_SESSION['isLoggedIn'] == TRUE) {

            $cdata['mysession'] = $_SESSION;
            $mp['startpage'] = $this->load->view('layout/emp/startpage', '', TRUE);
            $mp['menu'] = $this->load->view('layout/emp/menu', $_SESSION, TRUE);
            $mp['content'] = $this->load->view('employee/' . $cview, $cdata, TRUE);
            $mp['footer'] = $this->load->view('layout/emp/footer', '', TRUE);
            $mp['endpage'] = $this->load->view('layout/emp/endpage', '', TRUE);

            $this->load->view('employee/empmainpage', $mp);
        } else {
            $this->logout();
        }
    }

    //    **************************************************    Branch to api
    public function showBranchByID($id) {
        $xapi = gUrl('showbyid/' . $id, 'editbranch/');
        // debug($xapi);
        // exit();
        $cdata['chk'] = 'showBranchByID';
        $cdata['shbranch'] = cUrl($xapi . '?param=true', 'get');

        $this->load->view('employee/showdatahtml', $cdata);
    }

    public function deleteBranchByID($id) {
        if ($_SESSION['isLoggedIn'] == TRUE) {
            $xapi = gUrl('showbyid/' . $id, 'delbranch/');
            $cdata['chk'] = 'deleteBranchByID';
            $cdata['shbranch'] = cUrl($xapi . '?param=true', 'get');
            $cdata['mysession'] = $_SESSION;

            $this->load->view('employee/showdatahtml', $cdata);
        } else {
            redirect('empmain/M0001');
        }
    }

//    **************************************************    Customer to api
    public function saveCustomer() {
        $post = $this->input->post();

        // exit();
        // $this->load->helper(array('form', 'url'));
        // $this->load->library('form_validation');
        // $this->form_validation->set_rules('CUSidc', 'CUSidc', 'required');
        // $this->form_validation->set_rules('CUStitle', 'CUStitle', 'required');
        // $this->form_validation->set_rules('CUSfname', 'CUSfname', 'required');
        // $this->form_validation->set_rules('CUSlname', 'CUSlname', 'required');
        // $this->form_validation->set_rules('CUSadr', 'CUSadr', 'required');
        // $this->form_validation->set_rules('CUSzipc', 'CUSzipc', 'required');
        // $this->form_validation->set_rules('CUSbday', 'CUSbday', 'required');
        // $this->form_validation->set_rules('CUSemail', 'CUSemail', 'valid_email');
        // $this->form_validation->set_rules('CUSnphone', 'CUSnphone', 'required|integer');
        // $this->form_validation->set_rules('CUSbrhid', 'CUSbrhid', 'required');
        $countEmail = $this->SystemModel->CheckSumCUS($post['CUSemail']);

        if ($post) {
            if ($_SESSION['isLoggedIn'] == TRUE) {

                $PERsession = $_SESSION;
                $np = date("Ymd", strtotime($post['CUSbday']));

                $data = array(
                    'CUSregonweb' => '0',
                    'CUSidc' => $post['CUSidc'],
                    'CUStitle' => $post['CUStitle'],
                    'CUSfname' => $post['CUSfname'],
                    'CUSlname' => $post['CUSlname'],
                    'CUSadr' => $post['CUSadr'],
                    'CUSzipc' => $post['CUSzipc'],
                    'CUSbday' => $post['CUSbday'],
                    'CUSemail' => $post['CUSemail'],
                    'CUSnphone' => $post['CUSnphone'],
                    'CUStype' => $post['CUStype'],
                    'CUSuname' => $post['CUSemail'],
                    'CUSpawo' => $np,
                    'CUSbrhid' => $PERsession['brhid'], //session
                    'CUScreatedBy' => $PERsession['code'], //session
                );

                if ($countEmail == 0) {
                    // $xapi = gUrl('/scustomer', '/savecustomer');
                    $mypost = cUrl($this->config->item('apiScustomer'), 'post', $data);

                    $this->session->set_flashdata('success', 'All the data is correct. Data is complete.');
                } else {
                    $this->session->set_flashdata('error', 'Please check your email. Can not record your email.');
                }
            } else {
                $this->session->set_flashdata('error', 'Please check your data. Can not record your data.');
            }
        } else {
            $this->session->set_flashdata('error', 'Please check your data. Can not record your data.');
        }
    }

    public function showCustomerByID($id) {

        $xapi = $this->config->item('apiShoCustomer') . $id;
        // debug($xapi);
        // exit();
        $cdata['chk'] = 'showCustomerByID';
        $cdata['shcustomer'] = cUrl($xapi . '?param=true', 'get');
        $cdata['dcustype'] = $this->SystemModel->DefaultUsecase('11', json_decode($cdata['shcustomer'], true)['data']['CUStype']);
        $cdata['dtitlename'] = $this->SystemModel->DefaultUsecase('13', json_decode($cdata['shcustomer'], true)['data']['CUStitle']);
        $cdata['custype'] = $this->SystemModel->Usecase('11');
        $cdata['titlename'] = $this->SystemModel->Usecase('13');
        $cdata['mysession'] = $_SESSION;

        $this->load->view('employee/showdatahtml', $cdata);
    }

    public function deleteCustomerByID($id) {
        if ($_SESSION['isLoggedIn'] == TRUE) {
            $xapi = $this->config->item('apiShoCustomer'). $id;
            $cdata['chk'] = 'deleteCustomerByID';

            $cdata['shcustomer'] = cUrl($xapi . '?param=true', 'get');
            $cdata['mysession'] = $_SESSION;

            $this->load->view('employee/showdatahtml', $cdata);
        } else {
            redirect('empmain/M0002');
        }
    }

    public function updateRoomFromAPI() {
        $rapi = adminAPI('get_rooms');
        $cdata['room'] = cUrlAdmin($rapi);
        $dapi = json_decode($cdata['room'], TRUE)['data'];
        echo '<pre>';
        print_r($dapi);
        echo '</pre>';
        exit();
        $countDapi = count($dapi);

        $roomtotal = $this->SystemModel->CheckTotalRoom();

        print_r($countDapi);
        echo '<br>';
        print_r($roomtotal);
        echo '<br>';

        if ($countDapi >= $roomtotal) {
            echo 'Data from API > Data from My Room';
        }elseif ($countDapi <= $roomtotal) {

        }

        foreach ($dapi as $rkey => $rvalue) {
            $sroom = $this->SystemModel->CheckSumRoom($rvalue['branchid'], $rvalue['roomnumber']);

            if ($sroom == 0) {

                echo '<br>';
                echo 'Save to tb == 0  ***>  ' . $rvalue['roomnumber'] . '        is     ' . $sroom;
//                $data = array(
//                    'ROMcode' => $rvalue['id'],
//                    'ROMno' => $rvalue['roomnumber'],
//                    'ROMpomid' => '',
//                    'ROMdescTH' => '',
//                    'ROMdescEN' => '',
//                    'ROMnature' => '',
//                    'ROMtype' => $rvalue['roomtype'],
//                    'ROMsubtype' => $rvalue['roomsubtypeid'],
//                    'ROMsta' => $rvalue['roomstatus'],
//                    'ROMrasid' => '',
//                    'ROMlimit' => '',
//                    'ROMpice' => '',
//                    'ROMpic' => '',
//                    'ROMplan' => '',
//                    'ROMbrhid' => $rvalue['branchid'],
//                    'ROMcreatedDT' => date("Y/m/d H:i:s", strtotime($rvalue['create_date'])),
//                    'ROMeditedDT' => date('Y-m-d H:i:s'),
//                    'ROMdelete' => '0',
//                    'ROMdeleteBy' => 'FAPI',
//                    'ROMactiveDT' => date('Y-m-d H:i:s')
//                );
//                $this->RoomModel->SaveDataFromAPI($data);
            } else {
                echo '<br>';
                echo '$sroom > 1  ***>  ' . $rvalue['roomnumber'] . '        is     ' . $sroom;
//                $data = array(
//                    'ROMno' => $rvalue['roomnumber'],
//                    'ROMbrhid' => $rvalue['branchid']
//                );
//                echo '<pre>';
//                print_r($data);
//                echo '</pre>';
//                exit();
//                $this->
            }
        }
    }

}
