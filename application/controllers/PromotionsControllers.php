<?php

class PromotionsControllers extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        if(!isset($_COOKIE['lang'])) {
            $lang = 'english';
            setcookie('lang', $lang);
        } else {
            $lang = $_COOKIE['lang'];
        }
        $this->lang->load($lang, $lang);
        
        $glr['startpage'] = $this->load->view('layout/startpage', '', TRUE);
        $glr['topmenu'] = $this->load->view('layout/topmenu', '', TRUE);
        
        $glr['footer'] = $this->load->view('layout/footer', '', TRUE);
        $glr['endpage'] = $this->load->view('layout/endpage', '', TRUE);
        $this->load->view('user/promotions', $glr);
//         $this->load->view('user/promotions');
    }
    
}
