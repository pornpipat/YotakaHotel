<?php

/*
 * Object
 */
$lang['close'] = 'ยกเลิก';
$lang['delete'] = 'ลบ';
$lang['notdelete'] = 'ไม่สามารลบได้';
$lang['deleteselect'] = 'คุณต้องการลบข้อมูลที่เลือกหรือไม่';
$lang['save'] = 'บันทึก';
$lang['edit'] = 'แก้ไข';
$lang['continue'] = 'ดำเนินการ';
$lang['confdeletestart'] = 'คุณต้องการลบ';
$lang['confdeleteend'] = 'หรือไม่';
$lang['select'] = 'เลือก';
$lang['nodelete'] = 'ไม่มีรายการที่เลือก';
$lang['mypic'] = 'รูปภาพ';
$lang['signout'] = 'ออกจากระบบ';
$lang['signin'] = 'เข้าสู่ระบบ';
$lang['editprofile'] = 'แก้ไขข้อมูลส่วนตัว';
$lang['sent'] = 'ส่ง';
$lang['search'] = 'ค้นหา';
$lang['showmore'] = 'แสดงข้อมูลเพิ่มเติม';
$lang['no'] = 'ลำดับ';
$lang['confirmdeletion'] = 'ยืนยันการลบ';
$lang['selectitem'] = 'เลือกรายการ';


//  -------------------------------------------------- * Top menu
$lang['home'] = 'หน้าแรก';
$lang['aboutas'] = 'เกี่ยวกับเรา';
$lang['promotions'] = 'โปรโมชั่น';
$lang['gallery'] = 'แกลลอรี่';
$lang['contactus'] = 'ติดต่อเรา';
$lang['booking'] = 'จองที่พัก';

#  Modal user
$lang['user'] = 'ผู้ใช้';
$lang['username'] = 'ชื่อผู้ใช้งาน';
$lang['password'] = 'รหัสผ่าน';
$lang['passwordconf'] = 'ยืนยันรหัสผ่าน';
$lang['forgot_your_password'] = 'ลืมรหัสผ่าน';
$lang['register'] = 'ลงทะเบียน';
//$lang['please_sign_in'] = 'กรุณาเข้าสู่ระบบ';
#  Dropdown language
$lang['please_select_language'] = 'กรุณาเลือกภาษา';
$lang['english'] = 'อังกฤษ';
$lang['thailand'] = 'ไทย';
$lang['chinese'] = 'จีน';
$lang['japanese'] = 'ญี่ปุ่น';

#  Modal forgot your password
$lang['write_your_email_here'] = 'เขียนอีเมลของคุณที่นี่';

#  Modal register
#  ???????
//  -------------------------------------------------- * Top menu
#####################################
//  -------------------------------------------------- * Footer
$lang['contact'] = 'ผู้ติดต่อ';
$lang['yotaka_adr'] = '264/5 ซอยมหาดไทย รามคำแหง 65 ลาดพร้าว 122 วังทองหลาง กรุงเทพมหานคร';
$lang['about'] = 'เกี่ยวกับ';
$lang['usefullinks'] = 'ลิงค์เพิ่มเติม';
//  -------------------------------------------------- * Footer
#####################################
//  -------------------------------------------------- * Contact us
$lang['comments'] = 'คำติชม';
$lang['cname'] = 'ชื่อ - นามสกุล';
$lang['vote'] = 'คะแนนความพึงพอใจ';
$lang['status'] = 'สะถานะ';
//  -------------------------------------------------- * Contact us

//  -------------------------------------------------- * Register
$lang['regisheading'] = 'ลงทะเบียน';
$lang['idcard'] = 'เลขที่บัตรประชาชน / เลขที่หนังสือเดินทาง';
$lang['titlename'] = 'คำนำหน้าชื่อ';
$lang['fristname'] = 'ชื่อ';
$lang['lastname'] = 'นามสกุล';
$lang['address'] = 'ที่อยู่';
$lang['brithday'] = 'วันเดือนปี เกิด';
$lang['email'] = 'อีเมล';
$lang['phonenumber'] = 'เบอร์โทรศัพท์';
$lang['confirmpassword'] = 'กรุณากรอกรหัสผ่านอีกครั้ง';
//  -------------------------------------------------- * Register

//  -------------------------------------------------- * Branch
$lang['branchcode'] = 'รหัสสาขา';
$lang['vatnumber'] = 'เลขที่ผู้เสียภาษี';
$lang['branchnameEN'] = 'ชื่อสาขา ENG';
$lang['branchnameTH'] = 'ชื่อสาขา TH';
$lang['establishment'] = 'วันที่ก่อตั้ง';
$lang['logopicture'] = 'เครื่องหมายการค้า';
$lang['zipcode'] = 'รหัสไปรษณีย์';
$lang['editbranch'] = 'แก้ไขข้อมูลสาขา';
$lang['delbranch'] = 'ลบข้อมูลสาขา';
$lang['branchhead'] = 'เพิ่มข้อมูลสาขา';
//  -------------------------------------------------- * Branch

//  -------------------------------------------------- * Customer
$lang['customomerhead'] = 'เพิ่มข้อมูลลูกค้า';
$lang['customomerheadedit'] = 'แก้ไขข้อมูลลูกค้า';
$lang['customomercode'] = 'รหัสลูกค้า';
$lang['customomername'] = 'ชื่อลูกค้า';
$lang['customomertype'] = 'ประเภทลูกค้า';
$lang['delcustomer'] = 'ลบข้อมูลลูกค้า';

//  -------------------------------------------------- * Customer

//  -------------------------------------------------- * booking
$lang['branchname'] = 'ชื่อสาขา';
$lang['checkin'] = 'วันที่เข้าพัก';
$lang['noofguest'] = 'จำนวนผู้เข้าพัก';
$lang['bbooking'] = 'จองห้องพัก';
$lang['description'] = 'ลักษณะ';
$lang['details'] = 'รายละเอียด';
$lang['note'] = 'คำอธิบาย';
$lang['editdate'] = 'วันที่แก้ไข';
$lang['selectbranch'] = 'กรุณาเลือกสาขา';
$lang['descriptth'] = 'คำอธิคำอธิบาย TH';
$lang['descripten'] = 'คำอธิคำอธิบาย EN';
$lang['detailsth'] = 'รายละเอียด TH';
$lang['detailsen'] = 'รายละเอียด EN';
$lang['releasedate'] = 'วันที่เผยแพร่';
$lang['roomtype'] = 'ประเภทห้อง';
//  -------------------------------------------------- * booking


//  -------------------------------------------------- * about as
$lang['title'] = 'ชื่อเรื่อง';
$lang['story'] = 'เนื้อเรื่อง';
$lang['titleth'] = 'ชื่อเรื่อง TH';
$lang['titleen'] = 'ชื่อเรื่อง EN';
$lang['storyth'] = 'เนื้อเรื่อง TH';
$lang['storyen'] = 'เนื้อเรื่อง EN';
//  -------------------------------------------------- * about as

//  -------------------------------------------------- * gallery
$lang['groupno'] = 'รหัสกลุ่ม';
$lang['groupnameth'] = 'ชื่อกลุ่ม TH';
$lang['groupnameen'] = 'ชื่อกลุ่ม EN';
$lang['groupname'] = 'ชื่อกลุ่ม';
$lang['pleaseaddgroup'] = 'กรุณาเพิ่มกลุ่ม';
$lang['grouphead'] = 'เพิ่มกลุ่ม';
$lang['groupedit'] = 'แก้ไขกลุ่ม';
$lang['groupdelete'] = 'ลบกลุ่ม';
//  -------------------------------------------------- * gallery

//  -------------------------------------------------- * slide
$lang['slidehead'] = 'เพิ่มรูปภาพ';
//  -------------------------------------------------- * slide

//  -------------------------------------------------- * comment
$lang['commenthead'] = 'ตอบกลับความคิดเห็น';
$lang['reply'] = 'ตอบกลับ';
//  -------------------------------------------------- * comment

//  -------------------------------------------------- * Content
$lang['contenthead'] = 'เพิ่มข้อมูล Content';
$lang['contentedit'] = 'แก้ไข Content';
$lang['contentdeletet'] = 'ลบ Content';
//  -------------------------------------------------- * Content
