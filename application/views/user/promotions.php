<!DOCTYPE html>
<html>
    <head>
        <?php echo $startpage; ?>
        <link href="<?php echo base_url(); ?>assets/calendar/fullcalendar.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/calendar/fullcalendar.print.min.css" rel='stylesheet' media='print' />

        <script src="<?php echo base_url(); ?>assets/calendar/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/calendar/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/calendar/fullcalendar.min.js"></script>
        <script>

            $(document).ready(function () {

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
//                        right: 'month,agendaWeek,agendaDay,listWeek'
                        right: 'month,agendaWeek,agendaDay'
                    },
                    defaultDate: '2018-06-21',
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        {
                            title: 'All Day Event5555',
                            start: '2018-06-01',
                            end: '2018-06-10',
                            url: 'http://google.com/'
                        },
                        {
                            title: 'Long Event',
                            start: '2018-06-07',
                            end: '2018-06-10'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2018-06-09T16:00:00'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2018-06-16T16:00:00'
                        },
                        {
                            title: 'Conference',
                            start: '2018-06-11',
                            end: '2018-06-13'
                        },
                        {
                            title: 'Meeting',
                            start: '2018-06-12T10:30:00',
                            end: '2018-06-12T12:30:00'
                        },
                        {
                            title: 'Lunch',
                            start: '2018-06-12T12:00:00'
                        },
                        {
                            title: 'Meeting',
                            start: '2018-06-12T14:30:00'
                        },
                        {
                            title: 'Happy Hour',
                            start: '2018-06-12T17:30:00'
                        },
                        {
                            title: 'Dinner',
                            start: '2018-06-12T20:00:00'
                        },
                        {
                            title: 'Birthday Party',
                            start: '2018-06-13T07:00:00'
                        },
                        {
                            title: 'Click for Google',
                            url: 'http://google.com/',
                            start: '2018-06-28'
                        }
                    ]
                });

            });

        </script>
        <style>
            /*            body {
                            margin: 40px 10px;
                            padding: 0;
                            font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
                            font-size: 14px;
                        }*/

            #calendar {
                max-width: 900px;
                margin: 0 auto;
                /* color: #18b9e6; */
            }
        </style>
    </head>
    <body>

        <?php echo $topmenu; ?>
        <h1 style="margin-top: 45px; text-align: center"><?php echo $this->lang->line("promotions"); ?></h1>

        <div class="container-fluid" style="max-width: 900px; margin: 0 auto;">
            <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-md-6">
                        <img src="https://tvlk.imgix.net/imageResource/2018/06/14/1528961056437-ec5ec2c8b94f2f8cff96937c96755f76.jpeg?auto=compress%2Cformat&cs=srgb&fm=pjpg&ixlib=java-1.1.1&q=75" style="width: 100%; height: auto; position: relative;">
                    </div>
                    <div class="col-md-6">
                        <img src="https://tvlk.imgix.net/imageResource/2018/05/04/1525427515613-ea01ee1c553ab148ed3397f58cd1f2d8.jpeg?auto=compress%2Cformat&cs=srgb&fm=pjpg&ixlib=java-1.1.1&q=75" style="width: 100%; height: auto; position: relative;">

                    </div>
                </div>
            <!-- </div> -->
            <!-- <div class="container-fluid" style="margin-top: 25px"> -->
                <div class="row" style="margin-top: 25px">
                    <div class=" col-md-6">
                        <img src="https://tvlk.imgix.net/imageResource/2018/03/31/1522519424279-441582dc7cfa5b7c086068c6737e5839.jpeg?auto=compress%2Cformat&cs=srgb&fm=pjpg&ixlib=java-1.1.1&q=75" style="width: 100%; height: auto; position: relative;">
                    </div>
                    <div class="col-md-6">
                        <img src="https://tvlk.imgix.net/imageResource/2018/04/05/1522941597762-14b83bf2b1af1f39363fbf7a064cbab2.jpeg?auto=compress%2Cformat&cs=srgb&fm=pjpg&ixlib=java-1.1.1&q=75" style="width: 100%; height: auto; position: relative;">
                    </div>
                </div>
            <!-- </div> -->
        </div>

        <div  id='calendar' style="margin-top: 50px"></div>



        <?php echo $footer; ?>
    </body>

    <?php // echo $endpage; ?>

    <script src="<?php echo base_url(); ?>assets/js/boostrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vender/popper.min.js"></script>

</html>

<!--https://fullcalendar.io/releases/fullcalendar/3.9.0/demos/agenda-views.html-->
<!--https://www.patchesoft.com/fullcalendar-with-php-and-codeigniter-->
