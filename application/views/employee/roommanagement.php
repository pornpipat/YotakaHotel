<?php
$branchname = json_decode($branch, TRUE)['data'];
if (!isset($_COOKIE["lang"])) {
    $lg = $lang;
} else {
    $lg = $_COOKIE["lang"];
}

if ($lg == 'thailand') {
    $sl = 'TH';
} else {
    $sl = 'EN';
}

echo '<pre>';
//print_r(json_decode($room, true));
//print_r(json_decode($datafromapi, true));
//print_r($branch['status']);
//print_r($branch['data']);
//debug($data);
echo '</pre>';

?>


<nav class="navbar navbar-expand-sm bg-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <div class="dropdown">
                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                    BRANCHS
                </button>
                <div class="dropdown-menu">
                    <?php foreach ($branchname as $branchkey => $branchvalue): ?>
                        <a class="dropdown-item" href="#"><?php echo $branchvalue['BRHdesc' . $sl]; ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </li>                                
        <li class="nav-item">
            <a class="nav-link" href="#">Link 3</a>
        </li>
    </ul>
</nav>


<!-- Example DataTables Card-->
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Room Management
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Delete</th>
                        <th><?php echo $this->lang->line("branchcode"); ?></th>
                        <th><?php echo $this->lang->line("vatnumber"); ?></th>
                        <th><?php echo $this->lang->line("branchname" . $sl); ?></th>
                        <th><?php echo $this->lang->line("address"); ?></th>
                        <th><?php echo $this->lang->line("establishment"); ?></th>
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>                   
                            <!--<button type="button" class="btn btn-warning btn-sm form-control" onclick="open_delgbranch_modal();"> <?php // echo $this->lang->line("delete"); ?> </button>-->
                        </th>
                        <th><?php echo $this->lang->line("branchcode"); ?></th>
                        <th><?php echo $this->lang->line("vatnumber"); ?></th>
                        <th><?php echo $this->lang->line("branchname" . $sl); ?></th>
                        <th><?php echo $this->lang->line("address"); ?></th>
                        <th><?php echo $this->lang->line("establishment"); ?></th>
                        <!--<th>Action</th>-->
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    if (isset(json_decode($datafromapi, true)['data'])) :
                        ?>
                        <?php foreach (json_decode($datafromapi, true)['data'] as $key => $value): ?>
                            <tr id="tr<?php echo $value['BRHid']; ?>" >
                                <th>
                                    <div class="form-check" >
                                        <label class="form-check-label">
                                            <?php if ($value['BRHid'] != 1) : ?>
                                                <input type="checkbox" id="<?php echo $value['BRHid']; ?>" class="form-check-input" value="<?php echo $value['BRHid']; ?>" name="chk" onchange="myFunction('tr' + this.id, this, this.id)"><?php echo $this->lang->line("select"); ?>
                                            <?php else : ?>
                                                <svg id="i-ban" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                                <circle cx="16" cy="16" r="14" />
                                                <path d="M6 6 L26 26" />
                                                </svg>
                                            <?php endif; ?>
                                        </label>
                                    </div>                                                                
                                </th>
                                <td><?php echo $value['BRHcode']; ?></td>
                                <td><?php echo $value['BRHvnum']; ?></td>
                                <td><?php echo $value['BRHdescTH']; ?></td>
                                <td><?php echo $value['BRHadr']; ?></td>
                                <td><?php
                                    $sdate = $value['BRHbday'];
                                    echo date("Y-m-d", strtotime($sdate));
                                    ?></td>
                                <!--<th>-->
                                    <!--<div class="btn-group form-control">-->
                                        <!--<button type="button" class="btn btn-info btn-sm form-control" onclick="open_editbranch_modal(<?php // echo $value['BRHid']; ?>);"> <?php // echo $this->lang->line("edit"); ?> </button>-->
                                        <?php // if ($value['BRHid'] != 1) : ?>
                                            <!--<button type="button" class="btn btn-warning btn-sm form-control" onclick="open_delbranch_modal(<?php // echo $value['BRHid']; ?>);""> <?php // echo $this->lang->line("delete"); ?> </button>-->
                                        <?php // endif; ?>
                                    <!--</div>-->
                                <!--</th>-->
                            </tr>                                                        
                            <?php
                        endforeach;
                    endif;
                    ?>

                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">
        <!--Updated yesterday at 11:59 PM-->
        <?php
        echo 'Updated  ' . $xdate;
        ?>
    </div>
</div>
<!-- /.container-fluid-->

<!-- /.container-fluid-->
<!-- /.content-wrapper-->