<?php
if (!isset($_COOKIE["lang"])) {
    $lg = $lang;
} else {
    $lg = $_COOKIE["lang"];
}

if ($lg == 'thailand') {
    $sl = 'TH';
} else {
    $sl = 'EN';
}

?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!--
<style>
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        border: 0px solid #ddd;
    }

    th, td {

        padding: 0px;
    }

    th:first-child, td:first-child {
        text-align: left;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    .fa-check {
        color: green;
    }

    .fa-remove {
        color: red;
    } */

    .carousel-inner img {
      width: 100%;
      height: 100%;
  }

 </style> -->

<?php
$branchname = json_decode($branch, true)['data'];

// debug($slpic);
?>

<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top:-13px">
    <ol class="carousel-indicators" style="visibility: hidden">
      <?php foreach ($slpic as $key => $value): ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php if($key == 0){echo 'class="active"';} ?>></li>
      <?php endforeach; ?>
    </ol>
    <div class="carousel-inner">
      <?php foreach ($slpic as $skey => $svalue): ?>
        <div class="carousel-item <?php if($skey == 0){echo "active";} ?>" style="height: auto">
            <img class="first-slide" src="<?php echo base_url(); ?>assets/img/slide/<?php echo $svalue['PICname'] ?>" style="position: relative; height: 800px;">
            <!-- <img class="first-slide img-fluid mx-auto d-block" src="<?php echo base_url(); ?>assets/img/slide/<?php echo $svalue['PICname'] ?>" alt="First slide" style="width: 100%; height: auto; position: relative;"> -->
        </div>
      <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <a class="carousel-indicators">
      <form name="from_chackbooking"enctype="multipart/form-data" id="from_chackbooking">
        <table>
            <tr>
                <th style="text-align:unset" width="30%">
                    <div class="form-group">
                        <label for="sel1" style="color: white"><i class="fa fa-map-marker" style="font-size:13px;">  BRANCH</i></label>
                          <select class="form-control" id="CBKbrhid" name="CBKbrhid" style="width:80%" onchange="selectbybranch(this.value)">
                            <option selected hidden></option>
                            <?php foreach ($branchname as $branchkey => $branchvalue): ?>
                            <option value="<?php echo $branchvalue['BRHid']; ?>"><?php echo $branchvalue['BRHdesc' . $sl]; ?></option>
                            <?php endforeach; ?>
                          </select>
                    </div>
                </th>
                <th style="text-align:unset" width="30%">
                  <div class="form-group">
                    <label for="sel1" style="color: white"><i class="fa fa-sliders" style="font-size:13px;">  TYPE ROOM</i></label>
                    <!-- <select class="form-control" id="CBKpeople" name="CBKpeople" style="width:80%">
                      <?php
                      for($i = 1; $i <= 20; $i++){
                        echo '<option>'. $i .'</option>';
                      }
                      ?>
                    </select> -->
                    <div class="dropdown">
                      <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
                        <?php echo $this->lang->line("selectitem"); ?>
                      </button>
                      <div class="dropdown-menu p-4" style="width:340px">
                        <div class="dash1" id="dash1">
                        </div>
                      </div>
                    </div>
                  </div>
                </th>
                <th style="text-align:unset" width="30%">
                    <div class="form-group">
                      <label for="sel1" style="color: white"><i class="fa fa-calendar" style="font-size:13px;">  CHECK IN / CHECK OUT</i></label>
                      <input class="form-control" type="text" id="bdaterange" name="bdaterange" value="<?php date("Y/m/d") ." - " . date("Y/m/d"); ?>" style="width:80%" />
                    </div>
                </th>
                <th style="text-align:unset" width="10%">
                    <button type="button" class="btn btn-warning form-control" onclick="chk_booking();" style="margin-top:15px;">BOOK NOW</button>
                </th>
            </tr>
        </table>
      </form>
    </a>
</div>



<script>
    $(function() {
      $('input[name="bdaterange"]').daterangepicker({
        opens: 'left'
      }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
    });

    function selectbybranch(value){
      // var value = selectObject.value;
      console.log(value);
      if (value != 0 || value != '') {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>sbtypesubtype/" + value + "?param=true",
            success: function (data) {
                $("#dash1").html(data);
            },
            error: function (err) {
                console.log(err);
            }
        });
      }

    }

</script>
