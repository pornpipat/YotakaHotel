<?php
if (!isset($_COOKIE["lang"])) {
    $lg = $lang;
} else {
    $lg = $_COOKIE["lang"];
}

if ($lg == 'thailand') {
    $sl = 'TH';
} else {
    $sl = 'EN';
}

// echo '<pre>';
//print_r($mysession);
//print_r(json_decode($shcustomer, true)['data']);
//debug(json_decode($shbranch, true));
//debug(json_decode($shcustomer, true)['data']);
// debug($shbranch);
// debug(json_decode($branch, true)['data']);
// print_r(json_decode($shpicture, true));
// echo '</pre>';
// debug($status);
// exit();

switch ($chk) {
    case 'showBranchByID':
        ?>
        <!-- Modal body -->
        <form name="from_ebranchmanagement"enctype="multipart/form-data" id="from_branchmanagement">
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden"  class="form-control" id="editBRHid" name="editBRHid" value="<?php echo json_decode($shbranch, true)['data']['BRHid']; ?>">
                    <label for="editBRHcode"><?php echo $this->lang->line("branchcode"); ?>:</label>
                    <input type="text"  class="form-control" id="editBRHcode" name="editBRHcode" value="<?php echo json_decode($shbranch, true)['data']['BRHcode']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editBRHdescEN"><?php echo $this->lang->line("branchnameEN"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHdescEN" name="editBRHdescEN" value="<?php echo json_decode($shbranch, true)['data']['BRHdescEN']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editBRHdescTH"><?php echo $this->lang->line("branchnameTH"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHdescTH" name="editBRHdescTH" value="<?php echo json_decode($shbranch, true)['data']['BRHdescTH']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editBRHadr"><?php echo $this->lang->line("address"); ?>:</label>
                    <textarea class="form-control" rows="3" id="editBRHadr" name="editBRHadr" required><?php echo json_decode($shbranch, true)['data']['BRHadr']; ?></textarea>
                </div>

                <?php
                if (strlen(json_decode($shbranch, true)['data']['BRHpic']) > 3) {
                    $name = json_decode($shbranch, true)['data']['BRHpic'];
                } else {
                    $name = 'no-image.png';
                }
                ?>
                <div class="form-group">
                    <label for="editBRHpic"><?php echo $this->lang->line("logopicture"); ?>:</label>
                </div>
                <div align="center">
                    <img src="<?php echo base_url() . 'assets/img/uploads/' . $name ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
                </div>
                <div class="form-group" style="margin-top: 15px">
                    <input type="file" class="form-control-file border" name="editBRHpic" id="editBRHpic">
                    <input type="hidden" class="form-control" id="eBRHpic" name="eBRHpic" value="<?php echo $name; ?>" >
                </div>

                <div class="form-group">
                    <label for="editBRHzipc"><?php echo $this->lang->line("zipcode"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHzipc" name="editBRHzipc" value="<?php echo json_decode($shbranch, true)['data']['BRHzipc']; ?>" onkeypress="return numberOnly(event);" required>
                </div>
                <div class="form-group">
                    <label for="editBRHvnum"><?php echo $this->lang->line("vatnumber"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHvnum" name="editBRHvnum" value="<?php echo json_decode($shbranch, true)['data']['BRHvnum']; ?>" onkeypress="return numberOnly(event);" required>
                </div>
                <div class="form-group">
                    <?php
                    $sdate = json_decode($shbranch, true)['data']['BRHbday'];
                    ?>
                    <label for="editBRHbday"><?php echo $this->lang->line("establishment"); ?>:</label>
                    <!--<input type="text" class="form-control" id="BRHbday" name="BRHbday" value="12/45/3544" >-->
                    <input type="text" class="form-control sbt" id="eBRHbday" name="eBRHbday" value="<?php echo date("Y-m-d", strtotime($sdate)); ?>" readonly>
                    <div class="collapse">
                        <input type="date" class="form-control" id="editBRHbday" name="editBRHbday" min="1000-01-01" max="3000-12-31" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="editBRHemail"><?php echo $this->lang->line("email"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHemail" name="editBRHemail" value="<?php echo json_decode($shbranch, true)['data']['BRHemail']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editBRHnphone"><?php echo $this->lang->line("phonenumber"); ?>:</label>
                    <input type="text" class="form-control" id="editBRHnphone" name="editBRHnphone" value="<?php echo json_decode($shbranch, true)['data']['BRHnphone']; ?>" onkeypress="return numberOnly(event);" required>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
            </div>
        </form>
        <script>
            $(document).ready(function () {
                $(".sbt").click(function () {
                    $(".collapse").collapse('toggle');
                });
            });
        </script>

        <?php
        break;
    case 'deleteBranchByID':
        ?>
        <!-- Modal body -->
        <form name="from_dbranchmanagement"enctype="multipart/form-data" id="from_branchmanagement">
            <div class="modal-body">
                <input type="hidden"  class="form-control" id="delBRHid" name="delBRHid" value="<?php echo json_decode($shbranch, true)['data']['BRHid']; ?>">
                <input type="hidden"  class="form-control" id="delPERid" name="delPERid" value="<?php echo $mysession['id']; ?>">
                <?php echo $this->lang->line("confdeletestart") . '   <b>' . json_decode($shbranch, true)['data']['BRHdesc' . $sl] . '</b>   ' . $this->lang->line("confdeleteend") ?>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="del_data();"><?php echo $this->lang->line("delete"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
            </div>
        </form>
        <?php
        break;
    case 'showCustomerByID':
        $cus = json_decode($shcustomer, true)['data'];
        ?>
        <form name="from_ecustomerhmanagement"enctype="multipart/form-data" id="from_ecustomerhmanagement">
            <!-- Modal body -->
            <div class="modal-body">

                <div class="form-group">
                    <label for="editCUSpic"><?php echo $this->lang->line("mypic"); ?>:</label>
                </div>
                <div align="center">
                    <img src="<?php echo base_url() . 'assets/img/uploads/' . $cus['CUSpic']; ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
                </div>
                <div class="form-group" style="margin-top: 15px">
                    <input type="file" class="form-control-file border" name="editCUSpic" id="editCUSpic">
                    <input type="hidden" class="form-control" id="eCUSpic" name="eCUSpic" value="<?php echo $cus['CUSpic']; ?>" >
                </div>

                <div class="form-group">
                    <label for="editCUSidc"><?php echo $this->lang->line("idcard"); ?>:</label>
                    <input type="text"  class="form-control" id="editCUSidc" name="editCUSidc" value="<?php echo $cus['CUSidc']; ?>" onkeypress="return numberOnly(event);" required>
                </div>
                <div class="form-group">
                    <label for="editCUStitle"><?php echo $this->lang->line("titlename"); ?>:</label>
                    <select class="form-control" id="editCUStitle" name="editCUStitle" required>
                        <?php foreach ($titlename as $titlekey => $titlevalue): ?><option value="<?php echo $titlevalue['USCcode']; ?>" <?php
                            if ($dtitlename['USCcode'] == $titlevalue['USCcode']) {
                                echo 'selected';
                            }
                            ?>>
                                        <?php echo $titlevalue['USCdesc' . $sl]; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="editCUSfname"><?php echo $this->lang->line("fristname"); ?>:</label>
                    <input type="text" class="form-control" id="editCUSfname" name="editCUSfname" value="<?php echo $cus['CUSfname']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editCUSlname"><?php echo $this->lang->line("lastname"); ?>:</label>
                    <input type="text" class="form-control" id="editCUSlname" name="editCUSlname" value="<?php echo $cus['CUSlname']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editCUSadr"><?php echo $this->lang->line("address"); ?>:</label>
                    <textarea class="form-control" rows="3" id="editCUSadr" name="editCUSadr" required><?php echo $cus['CUSadr']; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="editCUSzipc"><?php echo $this->lang->line("zipcode"); ?>:</label>
                    <input type="text" class="form-control" id="editCUSzipc" name="editCUSzipc" value="<?php echo $cus['CUSzipc']; ?>" onkeypress="return numberOnly(event);" required>
                </div>
                <div class="form-group">
                    <label for="editCUSbday"><?php echo $this->lang->line("brithday"); ?>:</label>
                    <input type="date" class="form-control" id="editCUSbday" name="editCUSbday" min="1000-01-01" max="3000-12-31" value="<?php echo date("Y-m-d", strtotime($cus['CUSbday'])); ?>">
                </div>
                <div class="form-group">
                    <label for="editCUSemail"><?php echo $this->lang->line("email"); ?>:</label>
                    <input type="text" class="form-control" id="editCUSemail" name="editCUSemail" value="<?php echo $cus['CUSemail']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="editCUSnphone"><?php echo $this->lang->line("phonenumber"); ?>:</label>
                    <input type="text" class="form-control" id="editCUSnphone" name="editCUSnphone" value="<?php echo $cus['CUSnphone']; ?>" onkeypress="return numberOnly(event);"  required>
                </div>
                <div class="form-group">
                    <label for="editCUStype"><?php echo $this->lang->line("customomertype"); ?>:</label>
                    <select class="form-control" id="editCUStype" name="editCUStype" required>
                        <?php foreach ($custype as $custypekey => $custypevalue): ?>
                            <option value="<?php echo $custypevalue['USCcode']; ?>" <?php
                            if ($custypevalue['USCcode'] == $custypevalue['USCcode']) {
                                echo 'selected';
                            }
                            ?>>
                                        <?php echo $custypevalue['USCdesc' . $sl]; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id="editCUSbrhid" name="editCUSbrhid" value="<?php echo $mysession['id']; ?>" >
                    <input type="hidden" class="form-control" id="editCUSid" name="editCUSid" value="<?php echo $cus['CUSid']; ?>" >
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                </div>

            </div>

        </form>
        <?php
        break;
    case 'deleteCustomerByID':
        ?>
        <!-- Modal body -->
        <form name="from_dcustomermanagement"enctype="multipart/form-data" id="from_branchmanagement">
            <div class="modal-body">
                <input type="hidden"  class="form-control" id="delCUSid" name="delCUSid" value="<?php echo json_decode($shcustomer, true)['data']['CUSid']; ?>">
                <input type="hidden"  class="form-control" id="delPERid" name="delPERid" value="<?php echo $mysession['id']; ?>">
                <?php echo $this->lang->line("confdeletestart") . '   <b>' . json_decode($shcustomer, true)['data']['CUSfname'] . '   ' . json_decode($shcustomer, true)['data']['CUSlname'] . '</b>   ' . $this->lang->line("confdeleteend") ?>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="del_data();"><?php echo $this->lang->line("delete"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
            </div>
        </form>
        <?php
        break;
    case 'showbyidPicture':
      $branchname = json_decode($branch, true)['data'];
?>
    <!-- Modal body -->
    <form name="from_epicture"enctype="multipart/form-data" id="from_epicture">
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" class="form-control" id="editPICperid" name="editPICperid" value="<?php echo $mysession['id']; ?>" >
            <input type="hidden" class="form-control" id="editPICidtab" name="editPICidtab" value="103000" >
            <input type="hidden" class="form-control" id="editPICtype" name="editPICtype" value="1" >
            <input type="hidden" class="form-control" id="editPICid" name="editPICtype" value="<?php echo $shpicture['PICid']; ?>" >
          </div>
          <div class="form-group">
            <div class="dropdown">
              <label for="editPICsid">กรุณาเลือกสาขา:</label>
              <select class="form-control" id="editPICsid" name="editPICsid">
                <?php foreach ($branchname as $branchkey => $branchvalue): ?>
                <option value="<?php echo $branchvalue['BRHid']; ?>"
                  <?php if ($shpicture['PICsid'] == $branchvalue['BRHid']) {
                    echo 'selected';
                  } ?>
                  >
                  <?php echo $branchvalue['BRHdesc' . $sl]; ?>
                </option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
              <label for="editPICpic">กรุณาเลือกรูปภาพ:</label>
          </div>
          <div align="center">
              <img src="<?php echo base_url() . 'assets/img/uploads/' . $shpicture['PICname'] ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
          </div>
          <div class="form-group" style="margin-top: 15px">
              <input type="file" class="form-control-file border" name="editPICpic" id="editPICpic">
              <input type="hidden" class="form-control" id="ePICpic" name="ePICpic" value="<?php echo $shpicture['PICname']; ?>" >
          </div>
          <div class="form-group">
            <label for="editPICnote">คำอธิบายรูปภาพ:</label>
            <textarea class="form-control" rows="5" id="editPICnote" name="editPICnote"><?php echo $shpicture['PICnote']; ?></textarea>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
        </div>
    </form>
<?php
      break;
      case 'delbyidPicture':
?>
      <!-- Modal body -->
      <form name="from_dpicture"enctype="multipart/form-data" id="from_branchmanagement">
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" class="form-control" id="delPICperid" name="delPICperid" value="<?php echo $mysession['id']; ?>" >
            <input type="hidden" class="form-control" id="delPICidtab" name="delPICidtab" value="103000" >
            <input type="hidden" class="form-control" id="delPICtype" name="delPICtype" value="1" >
            <input type="hidden" class="form-control" id="delPICid" name="delPICid" value="<?php echo $shpicture['PICid']; ?>" >
          </div>
          <div align="center">
              <img src="<?php echo base_url() . 'assets/img/uploads/' . $shpicture['PICname'] ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
          </div>
          <div class="form-group">
            <label for="editPICnote">คำอธิบายรูปภาพ:</label>
            <textarea class="form-control" rows="5" id="editPICnote" name="editPICnote" readonly><?php echo $shpicture['PICnote']; ?></textarea>
          </div>

        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="del_data();"><?php echo $this->lang->line("save"); ?></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
        </div>
      </form>
<?php
        break;
        case 'showbyidPageBooking':
        $branchname = json_decode($branch, true)['data'];
        // debug($status);
?>
            <!-- Modal body -->
            <form name="from_pbooking"enctype="multipart/form-data" id="from_pbooking">
              <div class="modal-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="editPU03perid" name="editPU03perid" value="<?php echo $mysession['id']; ?>" >
                  <input type="hidden" class="form-control" id="editPU03id" name="editPU03id" value="<?php echo $shpagebooking['PU03id']; ?>" >
                  <!-- <input type="text" class="form-control" id="editPU03brhid" name="editPU03brhid" value="<?php //echo $shpagebooking['PU03brhid']; ?>" > -->
                </div>
                <div class="form-group">
                  <div class="dropdown">
                    <label for="editPU03brhid">กรุณาเลือกสาขา:</label>
                    <select class="form-control" id="editPU03brhid" name="editPU03brhid">
                      <?php foreach ($branchname as $branchkey => $branchvalue): ?>
                      <option value="<?php echo $branchvalue['BRHid']; ?>"
                        <?php if ($shpagebooking['PU03id'] == $branchvalue['BRHid']) {
                          echo 'selected';
                        } ?>
                        >
                        <?php echo $branchvalue['BRHdesc' . $sl]; ?>
                      </option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="dropdown">
                    <label for="editPU03sta">สถานะ:</label>
                    <select class="form-control" id="editPU03sta" name="editPU03sta">
                      <?php foreach ($status as $skey => $svalue): ?>
                      <option value="<?php echo $svalue['USCcode']; ?>"
                        <?php if ($shpagebooking['PU03sta'] == $svalue['USCcode']) {
                          echo 'selected';
                        } ?>
                        >
                        <?php echo $svalue['USCdesc' . $sl]; ?>
                      </option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="editPU03descTH">คำอธิบายภาษาไทย:</label>
                  <textarea class="form-control" rows="2" id="editPU03descTH" name="editPU03descTH"><?php echo $shpagebooking['PU03descTH']; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="PU03descEN">คำอธิบายภาษาอังกฤษ:</label>
                  <textarea class="form-control" rows="2" id="editPU03descEN" name="editPU03descEN"><?php echo $shpagebooking['PU03descEN']; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="editPU03noteTH">คำอธิบายเพิ่มเติมภาษาไทย:</label>
                  <textarea class="form-control" rows="5" id="editPU03noteTH" name="editPU03noteTH"><?php echo $shpagebooking['PU03noteTH']; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="editPU03noteEN">คำอธิบายเพิ่มเติมภาษาอังกฤษ:</label>
                  <textarea class="form-control" rows="5" id="editPU03noteEN" name="editPU03noteEN"><?php echo $shpagebooking['PU03noteEN']; ?></textarea>
                </div>
              </div>
              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
              </div>
            </form>

<?php
        break;
        case 'delbyidBookingUsePage':
?>
            <!-- Modal body -->
            <form name="from_dpicture"enctype="multipart/form-data" id="from_branchmanagement">
              <div class="modal-body">
                <div class="form-group">
                  <input type="text" class="form-control" id="delPU03perid" name="delPU03perid" value="<?php echo $mysession['id']; ?>" >
                  <input type="text" class="form-control" id="delPU03id" name="delPU03id" value="<?php echo $shpagebooking['PU03id']; ?>" >
                </div>
                <div align="center">
                  <?php echo $this->lang->line("confdeletestart") . '   <b>' . $shpagebooking['PU03desc'.$sl] . '   </b>' . $this->lang->line("confdeleteend") ?>
                </div>
              </div>
              <!-- Modal footer -->
              <div class="modal-footer">
                  <button type="button" class="btn btn-success" onclick="del_data();"><?php echo $this->lang->line("save"); ?></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
              </div>
            </form>

<?php
          break;

          case 'sentCommentToCustomer':
          // debug($mycomment);
          // debug($mysession);
?>
            <!-- Modal body -->
            <form name="from_comment"enctype="multipart/form-data" id="from_comment">
              <div class="modal-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="CMEperid" name="CMEperid" value="<?php echo $mysession['id']; ?>" >
                  <input type="hidden" class="form-control" id="CMEid" name="CMEid" value="<?php echo $mycomment['CMEid'] ?>" >
                  <input type="hidden" class="form-control" id="CMEvote" name="CMEvote" value="<?php echo $mycomment['CMEvote'] ?>" >
                  <input type="hidden" class="form-control" id="CMEcomdate" name="CMEcomdate" value="<?php echo date("Y-F-d", strtotime($mycomment['CMEcomdate'])) ?>" >
                </div>
                <div class="form-group">
                  <label for="CMEname">Name:</label>
                  <input type="text" class="form-control" id="CMEname" name="CMEname" value="<?php echo $mycomment['CMEname']; ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="CMEemail">Email:</label>
                  <input type="text" class="form-control" id="CMEemail" name="CMEemail" value="<?php echo $mycomment['CMEemail']; ?>" readonly>
                </div>
                <div class="form-group">
                  <label for="CMEcomment">Comment:</label>
                  <textarea class="form-control" rows="3" id="CMEcomment" name="CMEcomment" readonly><?php echo $mycomment['CMEcomment']; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="CMEresmessage">Reply:</label>
                  <textarea class="form-control" rows="5" id="CMEresmessage" name="CMEresmessage">Thanks for the comment.</textarea>
                </div>
              </div>
              <!-- Modal footer -->
              <div class="modal-footer">
                  <button type="button" class="btn btn-success" onclick="send_data();"><?php echo $this->lang->line("save"); ?></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
              </div>
            </form>
<?php
            break;

            case 'showbyidAboutas':
?>
            <!-- Modal body -->
            <form name="from_eaboutas"enctype="multipart/form-data" id="from_eaboutas">
                <div class="modal-body">
                  <?php foreach ($shaboutas as $akey => $avalue): ?>

                  <?php endforeach; ?>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="editPU01perid" name="editPU01perid" value="<?php echo $mysession['id']; ?>" >
                    <input type="hidden" class="form-control" id="editPU01id" name="editPU01id" value="<?php echo $shaboutas['PU01id']; ?>" >
                    <input type="hidden" class="form-control" id="editPICidtab" name="editPICidtab" value="980002" >
                    <input type="hidden" class="form-control" id="editPICtype" name="editPICtype" value="1" >
                    <input type="hidden" class="form-control" id="editPICid" name="editPICid" value="<?php echo $shaboutas['pic']['PICid']; ?>" >
                  </div>
                  <div class="form-group">
                    <label for="editPU01titleTH">Title TH:</label>
                    <input type="text" class="form-control" id="editPU01titleTH" name="editPU01titleTH" value="<?php echo $shaboutas['PU01titleTH']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="editPU01titleEN">Title EN:</label>
                    <input type="text" class="form-control" id="editPU01titleEN" name="editPU01titleEN" value="<?php echo $shaboutas['PU01titleEN']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="editPU01descTH">Content TH:</label>
                    <textarea class="form-control" rows="5" id="editPU01descTH" name="editPU01descTH"><?php echo $shaboutas['PU01descTH']; ?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="editPU01descEN">Content EN:</label>
                    <textarea class="form-control" rows="5" id="editPU01descEN" name="editPU01descEN"><?php echo $shaboutas['PU01descEN']; ?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="editPU01youtube">Link Youtube:</label>
                    <input type="text" class="form-control" id="editPU01youtube" name="editPU01youtube" value="<?php echo $shaboutas['PU01youtube']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="editPU01facebook">Link Facebook:</label>
                    <input type="text" class="form-control" id="editPU01facebook" name="editPU01facebook" value="<?php echo $shaboutas['PU01facebook']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="editPU01line">Link Line:</label>
                    <input type="text" class="form-control" id="editPU01line" name="editPU01line" value="<?php echo $shaboutas['PU01line']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="editPU01twitter">Link Twitter:</label>
                    <input type="text" class="form-control" id="editPU01twitter" name="editPU01twitter" value="<?php echo $shaboutas['PU01twitter']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="PICpic">รูปภาพ:</label>
                  </div>
                  <div align="center">
                      <img src="<?php echo base_url() . 'assets/img/uploads/' . $shaboutas['pic']['PICname'] ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236" />
                  </div>
                  <div class="form-group" style="margin-top: 20px">
                    <input type="file" class="form-control-file border" name="editPICpic" id="editPICpic">
                    <input type="hidden" class="form-control" id="ePICpic" name="ePICpic" value="<?php echo $shaboutas['pic']['PICname']; ?>" >
                  </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                </div>
            </form>
<?php
              break;

              case 'delbyidAboutas':
?>
              <!-- Modal body -->
              <form name="from_dpicture"enctype="multipart/form-data" id="from_branchmanagement">
                <div class="modal-body">
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="delPICperid" name="delPICperid" value="<?php echo $mysession['id']; ?>" >
                    <input type="hidden" class="form-control" id="delPU01id" name="delPU01id" value="<?php echo $shaboutas['PU01id']; ?>" >
                    <input type="hidden" class="form-control" id="delPICid" name="delPICid" value="<?php echo $shaboutas['pic']['PICid']; ?>" >
                    <?php echo $this->lang->line("confdeletestart") . '   <b>' . $shaboutas['PU01title'.$sl] . '</b>  '  . $this->lang->line("confdeleteend") ?>
                  </div>

                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="del_data();"><?php echo $this->lang->line("save"); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                </div>
              </form>
<?php
                break;

                case 'showGroupByImg':
?>

                  <?php if (count($sbbyimg) > 0): ?>
                    <div class="row" style="margin-top:20px">
                      <div class="col-sm-12">
                        <div class="card border border-danger">
                          <div class="card-body">
                            <label for="PICsid"><?php echo $this->lang->line("pleaseselectgroup"); ?></label>
                            <div class="row">
                              <?php foreach ($sbbyimg as $sbkey => $sbvalue): ?>
                              <div class="col col-sm-4">
                                <div class="form-check-inline">
                                  <label class="form-check-label" for="PICsid">
                                    <input type="radio" class="form-check-input" id="PICsid" name="PICsid" value="<?php echo $sbvalue['PU04id']; ?>"><?php echo $sbvalue['PU04descTH']; ?>
                                  </label>
                                </div>
                              </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12" style="margin-top:20px">
                        <div class="form-group">
                          <label for="PICpic"><?php echo $this->lang->line("mypic"); ?>:</label>
                          <input type="file" class="form-control-file border" name="PICpic" id="PICpic">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="PICnote"><?php echo $this->lang->line("note"); ?>:</label>
                          <textarea class="form-control" rows="5" id="PICnote" name="PICnote"></textarea>
                        </div>
                      </div>
                    </div>
                  <?php else: ?>
                    <div class="col-sm-12" style="margin-top:20px">
                      <div class="form-group">
                        <label ><?php echo $this->lang->line("pleaseaddgroup"); ?></label>
                      </div>
                    </div>
                  <?php endif; ?>

<?php
                  break;

                  case 'delbyidGallery':
                     $shgallery = $shgallery[0];
?>
                    <!-- Modal body -->
                    <form name="from_dpicture"enctype="multipart/form-data" id="from_branchmanagement">
                      <div class="modal-body">
                        <div class="form-group">
                          <input type="hidden" class="form-control" id="delPU04perid" name="delPU04perid" value="<?php echo $mysession['id']; ?>" >
                          <input type="hidden" class="form-control" id="delPU04idtab" name="delPU04idtab" value="980003" >
                          <input type="hidden" class="form-control" id="delPU04type" name="delPU04type" value="1" >
                          <input type="hidden" class="form-control" id="delPU04id" name="delPU04id" value="<?php echo $shgallery['PU04id']; ?>" >
                        </div>
                        <div align="center">
                          <?php echo $this->lang->line("confdeletestart") . '   <b>' . $shgallery['PU04desc'.$sl] . '   </b>' . $this->lang->line("confdeleteend") ?>
                        </div>

                      </div>
                      <!-- Modal footer -->
                      <div class="modal-footer">
                          <button type="button" class="btn btn-success" onclick="del_data();"><?php echo $this->lang->line("save"); ?></button>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                      </div>
                    </form>
                    <?php
                    break;

                    case 'showbyidGallery':
                       $shgallery = $shgallery[0];
?>
                    <!-- Modal body -->
                    <form name="from_dgallery"enctype="multipart/form-data" id="from_dgallery">
                      <div class="modal-body">
                        <div class="form-group">
                          <input type="hidden" class="form-control" id="editPU04perid" name="editPU04perid" value="<?php echo $mysession['id']; ?>" >
                          <input type="hidden" class="form-control" id="editPU04idtab" name="editPU04idtab" value="980003" >
                          <input type="hidden" class="form-control" id="editPU04type" name="editPU04type" value="1" >
                          <input type="hidden" class="form-control" id="editPU04id" name="editPU04id" value="<?php echo $shgallery['PU04id']; ?>" >
                        </div>
                        <div class="form-group">
                          <label for="editPU04descTH">ชื่อกลุ่ม TH :</label>
                          <input type="text" class="form-control" id="editPU04descTH" name="editPU04descTH" value="<?php echo $shgallery['PU04descTH']; ?>">
                        </div>
                        <div class="form-group">
                          <label for="editPU04descEN">ชื่อกลุ่ม EN :</label>
                          <input type="text" class="form-control" id="editPU04descEN" name="editPU04descEN" value="<?php echo $shgallery['PU04descEN']; ?>">
                        </div>
                        <div class="form-group">
                          <label for="editPU04note">คำอธิบายกลุ่ม:</label>
                          <textarea class="form-control" rows="5" id="editPU04note" name="editPU04note"><?php echo $shgallery['PU04note']; ?></textarea>
                        </div>
                      </div>

                      </div>
                      <!-- Modal footer -->
                      <div class="modal-footer">
                          <button type="button" class="btn btn-success" onclick="edit_data();"><?php echo $this->lang->line("save"); ?></button>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                      </div>
                    </form>
<?php
                      break;

                      case 'showDeletegroupgallery':
?>
                      <!-- Modal body -->
                      <form name="from_dpicture"enctype="multipart/form-data" id="from_branchmanagement">
                        <?php foreach ($shgallery as $key => $value): ?>
                        <div class="modal-body">
                          <div class="form-group">
                            <input type="hidden" class="form-control" id="gdelPU04perid" name="gdelPU04perid" value="<?php echo $mysession['id']; ?>" >
                            <input type="hidden" class="form-control" id="gdelPU04idtab" name="gdelPU04idtab" value="980003" >
                            <input type="hidden" class="form-control" id="gdelPU04type" name="gdelPU04type" value="1" >
                            <input type="hidden" class="form-control" id="gdelPU04id" name="gdelPU04id" value="<?php echo $value['PU04id']; ?>" >
                          </div>
                          <div align="center">
                            <?php echo $this->lang->line("confdeletestart") . '   <b>' . $value['PU04desc'.$sl] . '   </b>' . $this->lang->line("confdeleteend") ?>
                          </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" onclick="del_gdata();"><?php echo $this->lang->line("save"); ?></button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                        </div>
                        <?php endforeach; ?>
                      </form>
<?php
                        break;

    default:
        echo 'No data and tag HTML';

        break;
}
?>
